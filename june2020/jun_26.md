# 6/26 Camping at Algonquin Park - Day 2
We had a terrible night because of sounds of natural, wind, animals etc. But look at this, the morning at Algonquin Park was awesome. The air was so fresh, the sky was so bright. I love it.

![](jun_26_a.jpg)

![](jun_26_b.jpg)

The water was so clean and cold.
![](jun_26_c.jpg)

![](jun_26_d.jpg)

We saw a group of butterflies
![](jun_26_e.jpg)

There was a small island in the middle of the lake. We had gotten to there and took photos by drone.
![](jun_26_f.MOV)

And Ciel was fishing.
![](jun_26_g.jpg)

AJ controled his drone.
![](jun_26_h.jpg)

After that, we were fishing in the middle of the lake.
![](jun_26_i.jpg)

At the end, we had said goodbye to a chipmunk and we left there.
![](jun_26_j.mp4)
