# 6/20 Cherry Beach, Ontario, Canada

It's been a long time since the beach was open for people last time. AJ, Ciel and I visited the Cherry Beach today. In fact, it's a lake not the ocean, but it's huge that looks like an ocean. We got a wonderful day here. 

The Ontario lake
![](jun_20_a.jpg)

Everybody went here and relax.
![](jun_20_b.jpg)

We got some BBQ.
![](jun_20_c.jpg)

AJ and I
![](jun_20_d.jpg)

![](jun_20_e.jpg)

We were starving!!! Let's eat.
![](jun_20_f.jpg)

There was a music party around us, everyone just wnet to dance and enjoy the music.
![](jun_20_g.mp4)

We saw a beautiful sunset view when we were leaving.
![](jun_20_h.mp4)

After all, we got Church's chicken for dinner, which was recommanded from Ciel, she said this was really good when she tried it in Vancouver last time. And It is good, really.
![](jun_20_i.jpg)