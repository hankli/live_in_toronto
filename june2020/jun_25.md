# 6/25 Camping at Algonquin Park - Day 1

I even don't what's the last time I had a camping, but today we did it. AJ, Ciel and I were going to Algonquin Part to have a camping, how excited !!! It tooks two hours and half to arrived there. AJ rented a SUV for three of us so it's not a problem to take a 2.5-hour journey. We bought foods from Walmart on the way before we got there. And this's our first stop, Algonquin Visitor Center, we were going to ask some information we need.

![a](jun_25_a.jpg)

There is a wolf statue on the entrance.  
![b](jun_25_b.jpg)

It is a thank of donation.
![c](jun_25_c.jpg)

The map.
![d](jun_25_d.jpg)

And the photos.
![e](jun_25_e.jpg)

After asking, we got to small port. Here is where we rented the boat and we were going to find a place to camp for tonight. 
![f](jun_25_f.jpg)

This was the boat we rent
![g](jun_25_g.jpg)

We started BBQ after we found a great place. I know it looks like around 2 ~ 3 pm but it was 8 pm actually.
![l](jun_25_l.jpg)

We had steak and vegetables for our dinner.
![p](jun_25_p.jpg)

Night view, it was so beautiful.
![r](jun_25_r.jpg)

We moved to another place to chat each other and fired for cooking water.
![s](jun_25_s.jpg)


![u](jun_25_u.mp4)

It was a amazing night in Algonquin Park. 
![v](jun_25_v.jpg)