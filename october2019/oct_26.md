# 10/26

下午跟 Banjamin, Takeru 去看斷崖

### Bluffer's Park

![](oct_26_a.jpg)

進來後看見一片海灣
![](oct_26_b.jpg)

三人合照
![](oct_26_c.jpg)

前往斷崖的路上會先通過一個類似池塘的地方
![](oct_26_d.jpg)

斷崖本人
![](oct_26_g.jpg)

與斷崖合照
![](oct_26_e.jpg)

斷崖介紹
![](oct_26_f.jpg)

晚上去吃當地的達美樂
![](oct_26_h.jpg)

點了一份 XL (16 吋) 的大 Pizza，嗝～
![](oct_26_i.jpg)