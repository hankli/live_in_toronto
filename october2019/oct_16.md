# 10/16

- 陪同學開戶結果發現觀光簽不給辦，之後去 Art Gallery of Ontario，25 歲以下免費，門票 25 CAD，發現要錢後又改去咖啡廳聊天

- Banjamin 生日，晚餐去吃 Five Guys

### 跟 Amber, Tsubasa 去 Tim Hortons 聊天

![City Hall](oct_16_a.jpg)

### Banjamin's Birthday at Five Guys

Takeru, Banjamin, Hank
![City Hall](oct_16_b.jpg)

Five Guys 點餐蠻特別的，你可選擇三明治、熱狗、或漢堡，之後再選擇想要的配料，例如生菜、烤肉醬、洋蔥 等等... 
![City Hall](oct_16_c.jpg)

![City Hall](oct_16_d.jpg)

![City Hall](oct_16_f.jpg)

![City Hall](oct_16_e.jpg)

