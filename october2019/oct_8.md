# 10/8

早上正上課到 2:30，下午跟 Banjamin 出去晃晃

### Hang out with Banjamin
Union Station -> Scotiabank Arena -> Toronto Railway Museum -> Harbourfront

聯合車站
![Union Station](oct_8_a.jpg)

多倫多暴龍主場
![](oct_8_b.jpg)
![](oct_8_c.jpg)
![](oct_8_d.jpg)

同時也是冰球的球場
![](oct_8_e.jpg)

鐵道博物館(餐廳)
![](oct_8_f.jpg)
![](oct_8_g.jpg)
![](oct_8_l.jpg)

湖濱藝文中心看 CN Tower
![](oct_8_h.jpg)
![](oct_8_i.jpg)
![](oct_8_j.jpg)
![](oct_8_k.jpg)
