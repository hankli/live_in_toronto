# 10/6

第二天一早，想說剛到加拿大，先了解一下 HomeStay 附近有什麼東西，我住在 Etobicoke，搭地鐵到學校大約 45 ~ 60分鐘

出門先去買車票，馬上碰到難關，月票該買 TTC Monthly Downtown Express 還是 TTC Monthly，之後查了一下 TTC Monthly Downtown Express 有提供特殊的線路到其他城市，所以買 TTC Monthly 就足夠了

注意！車站內的販賣機只收 50 元以下，Shoppers drug market 可以用百鈔買

![中餐](./oct_6_a.jpg)

![三明治](./oct_6_b.jpg)

![星巴克 Toronto](./oct_6_c.jpg)

![星巴克 Canada](./oct_6_d.jpg)

![與學校拍照](./oct_6_e.jpg)

### 心得
- 加拿大人很熱情，幫忙服視障人士，幫陌生人撿東西，買東西會說 How are you, Have a nice day 等...

- 蜜蜂、鴿子超多