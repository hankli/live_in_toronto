# 10/31

[Halloween Street Party](https://www.blogto.com/fashion_style/2019/09/halloween-on-church-street/)

今天是萬聖節，很多學生穿著很酷炫的裝扮，晚上去參加萬聖節街頭派對

Today is Halloween, there are many students dress their customs up.

There is a street party on Church Street at 6:30

### In School

一、二名的南瓜作品 
![Top 2 curving pumpkins](oct_31_a.jpg)

其他的南瓜作品
![Other curving pupkins](oct_31_b.jpg)

金正恩
![The guy dresses up as 金正恩](oct_31_c.jpg)

其他的女同學
![The custom ladies](oct_31_d.jpg)

### Halloween Street Party

It looked like too early to Church Street, not too much people.
![](oct_31_e.jpg)

But later, people are more and more. There is a cute child with his parents.
![](oct_31_f.jpg)

A werewolf.
![](oct_31_g.jpg)

![](oct_31_h.jpg)

I was taken a picture with a guy who rode on Trump.
![](oct_31_i.jpg)