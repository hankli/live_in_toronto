# 10/19

跟 Abby, Tsubasa, Suzuki 約在 King Station 11:00 見面，我們去了

- St. Lawrence Market
- The Cathedral Church of St. James
- Berczy Park and Gooderham Building (just outside)
- Toronto's First Post Office

### [St. Lawrence Market](http://www.stlawrencemarket.com)

加拿大的傳統市場，裡面超多攤販賣很多美食，價格偏高但值得一逛

必嚐的食物
> - Carousel Bakery 麵包坊，招牌 Peameal Bacon Sandwich
> - Buster's Sea Cove 海鮮攤販
> - Currasco of St. Lawrence 葡式蛋塔
> - Uno Mustachio 茄子漢堡
> - Ice Wine 冰酒


![](oct_19_a.jpg)

因為是星期六來逛，人潮滿滿
![](oct_19_b.jpg)

Carousel Bakery 麵包坊
![](oct_19_c.jpg)

招牌 Peameal Bacon Sandwich
![](oct_19_d.jpg)

海鮮攤販
![](oct_19_e.jpg)

點了一份 HomeMade Crab Cakes + Fries + Chicken Sharmp Soup
![](oct_19_f.jpg)

Currasco of St. Lawrence 葡式蛋塔
![](oct_19_g.jpg)

### The Cathedral Church of St. James

![](oct_19_h.jpg)

![](oct_19_i.jpg)

### Berczy Park

![](oct_19_l.jpg)

![](oct_19_k.jpg)

### Gooderham Building

![](oct_19_m.jpg)

### Toronto's First Post Office

多倫多第一間郵局
![](oct_19_n.jpg)

![](oct_19_o.jpg)

![](oct_19_p.jpg)

![](oct_19_r.jpg)

![](oct_19_s.jpg)

二次世界大戰飛機
![](oct_19_t.jpg)

![](oct_19_q.jpg)