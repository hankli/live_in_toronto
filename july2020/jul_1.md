# 7/1 Toronto island

Since the covid-19 goes through all over the world, I'd stayed home for almost 4 months, finally, I was going to Toronto island when the gorvernment announces it was reopen on stage 2.

It needs to take water taxi to Toronto island, we got to here when we arrived Toronto island.
![](jul_1_a.jpg)

Look at this street signs. There are 548 km to New York city from here.
![](jul_1_b.jpg)

It was so hot on that day, I even can't see the sun directly.
![](jul_1_c.jpg)

And then we went to the beach on the Toronto island.
![](jul_1_d.jpg)

I remember that was so hot even the sand, everybody wants to play in the water.
![](jul_1_e.jpg)

After visited beach, we went forward to restaurant for lunch.
![](jul_1_h.mp4)

Before lunch, we want to take photos with CN tower first.
![](jul_1_f.jpg)

I'm happy I've been here, it's unforgettable.
![](jul_1_g.jpg)

AJ, Ciel and me.
![](jul_1_i.jpg)

Toronto Island BBQ restaurant.
![](jul_1_j.jpg)

I orderd a lobster sandwich.
![](jul_1_k.jpg)

After I leaved Toronto island, I went to meet May because I was gonna back to Taiwan tomorrow after that day.
![](jul_1_l.jpg)
