# 7/2 The last day in Canada of Working Holiday

Due to covid-19, I had to back to Taiwan earlier than I expected. Today was my last day of the working holiday. I want to say thank you to Athena and her family, who was my landlord and friend, thank you for taking care of me. She is the best landlord I've ever met in Toronto.

Kris, Athena, Eddie and me.
![](jul_2_a.jpg)

Kris's convertible sport car.
![](jul_2_b.jpg)

Let's go for a ride.
![](jul_2_c.mp4)

I was so excited because I've never had a ride by convertible sport car and also it's Audi.
![](jul_2_d.jpg)

We went through the park and just back home.
![](jul_2_e.jpg)

This was my roommate, Hearcules, we called "neighbor" to each other all the time.
![](jul_2_f.jpg)

After all, I got to the airport and went back to Taiwan
![](jul_2_g.jpg)

**I planed to stay for whole year in Canada but I've never though there is a coronavirus and it totally breaks down my plan but it's OK, I will be back someday in the future. Thank you everyone who takes care of me, helps me. See you next time.**

