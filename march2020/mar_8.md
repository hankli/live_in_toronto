# 3/8

## [Daldongnae Korean BBQ 달동네](https://goo.gl/maps/TsUtP98Lyr4oLr6t5)

Today, I went for BBQ in a Korean restaurant with my friends, this restaurant is good and I love BBQ so much.

![](mar_8_a.jpg)

![](mar_8_b.jpg)

## High Park

After lunch, we went to the high park, there were so many people walked their dog on that day. We walked into the park and visited the zoom in the high park, and ate the ice creams together.

![](mar_8_c.mp4)

![](mar_8_d.jpg)



