# 加拿大打工度假-多倫多

Hi，我是 Hank，一名 Android 開發者，在 32 歲抽中加拿大打工度假決定放手一博前往多倫多工作生活．

在此非常感謝我的老婆及家人給我支持與鼓勵，非常感謝你們，永遠愛你們❤️

## 為什麼參加打工度假？

我做了 4~5 年的工程師，因為想學好英文以及響往國外新創文化，讓我想試試出國生活，目標放在英語系國家，首選當然是美國，但我沒有錢跟方法去美國，只好作罷．

當時澳洲打工度假非常夯，但我年紀已經超過 31 歲，沒辦法參加澳洲的打工度假...😥

某天，朋友跟我說加拿大打工度假年齡限制在 35 歲，但需要抽籤，心想不然就試試看，反正也沒損失，抽中再說，沒想到我意外幸運，參加第一梯次抽籤兩個禮拜後就收到抽中的通知😄

申請流程就不在這邊贅述，請自行 Google．

## 來去多倫多打工度假

這邊記錄我的打工度假生活、遇到的問題

### 出發前你必須知道的事
- [目標篇](./some_things_you_should_know/about_what_you_want.md)
- [金錢篇](./some_things_you_should_know/about_money.md)
- [城市篇](./some_things_you_should_know/about_city.md)
- [語校篇](./some_things_you_should_know/about_school.md)
- [住宿篇](./some_things_you_should_know/about_accommodation.md)
- [工作篇](./some_things_you_should_know/about_job.md)
- [簽證篇](./some_things_you_should_know/about_visa.md)

### Life in Toronto

**October 2019**

- [10/5 第一天抵達多倫多](./october2019/oct_5.md)
- [10/6 閒逛](./october2019/oct_6.md)
- [10/7 開學](./october2019/oct_7.md)
- [10/8 聯合車站、多倫多暴龍主場、鐵道博物館(餐廳)、湖濱藝文中心看 CN Tower](./october2019/oct_8.md)
- [10/9 TD 開戶](./october2019/oct_9.md)
- [10/10 多倫多大學、皇后公園、安大略省議會大樓、皇家安大略博物館](./october2019/oct_10.md)
- [10/11 申請 SIN](./october2019/oct_11.md)
- [10/12 Yorkdale Shopping Center](./october2019/oct_12.md)
- [10/14 Vaughan outlet mall](./october2019/oct_14.md)
- [10/15 蔬果店](./october2019/oct_15.md)
- [10/16 跟同學聊天、Bajiman 生日](./october2019/oct_16.md)
- [10/17 認識新朋友 JOJO](./october2019/oct_17.md)
- [10/18 李泉居 台灣餐廳](./october2019/oct_18.md)
- [10/19 St. Lawrence Market, The Cathedral Church, Berczy Park, Gooderham Building, Toronto's First Post Office](./october2019/oct_19.md)
- [10/21 Game Coffee](./october2019/oct_21.md)
- [10/25 RIO 40 巴西餐廳](./october2019/oct_25.md)
- [10/26 Bluffer's Park](./october2019/oct_26.md)
- [10/29 Graffiti Alley](./october2019/oct_29.md)
- [10/31 Halloween Street Party](./october2019/oct_31.md)

**November 2019**

- [11/1 Last day of 11th session, SSAM Korean BBQ](./november2019/nov_1.md)
- [11/8 通堂拉麵](./november2019/nov_8.md)
- [11/9 二訪李泉居](./november2019/nov_9.md)
- [11/14 Lighting Ceremony in Eaton Center](./november2019/nov_14.md)
- [11/17 Seven Lives Tacos Y Mariscos](./november2019/nov_17.md)
- [11/20 Toronto Christmas Market](./november2019/nov_20.md)
- [11/28 Last day of 12th session, Chick-fil-A, Midtown Gastro Hub](./november2019/nov_28.md)

**December 2019**

- [12/3 The NBA game Raptors vs Heat](./december2019/dec_3.md)
- [12/12 Agency Party](./december2019/dec_12.md)
- [12/19 Last day of 13th session](./december2019/dec_19.md)
- [12/21 Blue Mountaion Snowboarding Trip - day1](./december2019/dec_21.md) 
- [12/22 Blue Mountaion Snowboarding Trip - day2](./december2019/dec_22.md)
- [12/23 Blue Mountaion Snowboarding Trip - day3](./december2019/dec_23.md)
- [12/27 Niagara Falls - day1](./december2019/dec_27.md)
- [12/28 Niagara Falls - day2](./december2019/dec_28.md)
- [12/31 New Year's Eve in Toronto](./december2019/dec_31.md)

**January 2020**

- [1/3 The last day in ILSC](./january2020/jan_3.md)
- [1/22 The NBA game Raptors vs 76ers](./january2020/jan_22.md)
- [1/23 The Distillery District](./january2020/jan_23.md)
- [1/25 Royal Ontario Museum, Ripley's Aquarium of Canada](./january2020/jan_25.md)
- [1/26 Casa Loma](./january2020/jan_26.md)
- [1/27 Niagara Falls - day1](./january2020/jan_27.md)
- [1/28 Niagara Falls - day2, CN tower](./january2020/jan_28.md)

**February 2020**

- [2/5 Koh Lipe Thai Kitchen](./february2020/feb_5.md)
- [2/14 The captain's boil](./february2020/feb_14.md)
- [2/27 GDG Toronto Android](./february2020/feb_27.md)
- [2/28 Having Dinner with housemates](./february2020/feb_28.md)

**March 2020**

- [3/1 Move to new house](./march2020/mar_1.md)
- [3/3 New job in Kinton Ramen](./march2020/mar_3.md)
- [3/8 Daldongnae Korean BBQ 달동네, High Park](./march2020/mar_8.md)
- [3/25 Be canceled all of the shifts](./march2020/mar_25.md)
- [3/26 Having Dinner with my neighbor](./march2020/mar_26.md)

**April 2020**

- [4/3 Cook braised pork at home](./april2020/apr_3.md)

**May 2020**

**June 2020**

- [6/20 BBQ at Cherry Beach](./june2020/jun_20.md)
- [6/24 Evergreen Brick Works](./june2020/jun_24.md)
- [6/25 Camping at Algonquin Park - day1](./june2020/jun_25.md)
- [6/25 Camping at Algonquin Park - day2](./june2020/jun_26.md)

**July 2020** 

- [7/1 Toronto island](./july2020/jul_1.md)
- [7/2	The last day in Canada of Working Holiday](./july2020/jul_2.md)