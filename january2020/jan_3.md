# 1/3

### The last day of ILSC

I'd studied three months in ILSC and today was my last day. I really enjoyed the time of school. I made the friends who come from different countries and I've learned from them a lot. I want to say thank you to all the teachers who taught me, even I still not very fluent. I'll do my best in the future. At last, I hope you guys all the best in future.

![](jan_3_a.jpg)

Wonderson, Noe, Jiyoon, and I. We completed the course of ILSC.
![](jan_3_b.jpg)

Abby, Lisa, and I
![](jan_3_e.jpg)

Yudai, and I
![](jan_3_c.jpg)

Chris, Sukuru, and I
![](jan_3_d.jpg)