# 1/22

My wife came to Toronto between 1/21 ~ 1/30, those were Chinese New Year Holidays.

### City Hall

![](jan_22_a.jpg)

### 通堂 ramen

Had beer and ramen for dinner. These beer was really good, just liked we were in Japan.
![](jan_22_b.jpg)

### NBA Game

This game was Raptors vs 76ers, this was my wife's first time to watch NAB game.
![](jan_22_c.jpg)

Abby, Katy, me.
![](jan_22_d.jpg)

Abby took a picture behind the court for us.
![](jan_22_e.jpg)