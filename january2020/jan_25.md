# 1/25


### [Royal Ontario Museum](https://www.rom.on.ca/en)

Dinosaur fossil
![](jan_25_a.jpg)

![](jan_25_b.jpg)

![](jan_25_c.jpg)

### [Ripley's Aquarium of Canada](https://www.ripleyaquariums.com/canada/)

![](jan_25_d.jpg)

![](jan_25_e.jpg)

Jellyfish
![](jan_25_f.mp4)
