# 1/28

### [Niagara Falls](https://www.niagaraparks.com/)

The journey behind the Falls.
![](jan_28_a.mp4)

There were just a few people there, we got a great place to take the picture.
![](jan_28_b.jpg)

### [CN Tower](https://www.cntower.ca/intro.html)

At night, we visited the CN tower.
![](jan_28_c.jpg)

The description of CN tower.
![](jan_28_d.jpg)

Toronto's night view.
![](jan_28_e.jpg)

![](jan_28_f.jpg)

![](jan_28_g.jpg)

This is the glass which I could see the ground, Looks very scary. 
![](jan_28_h.jpg)

Finally, we went donw to the second floor. Katy was happy.
![](jan_28_i.jpg)

