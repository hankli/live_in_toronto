# 1/26


### [Casa Loma](https://casaloma.ca/)

This is Toronto's majestic castle, there is art collection inside.

Front of door.
![](jan_26_a.jpg)

Room.
![](jan_26_b.jpg)

The carpet made by a tiger, I'm not sure it is real because the eyes looks like plastic balls.
![](jan_26_c.jpg)

![](jan_26_d.jpg)

Also vintage cars are.
![](jan_26_e.jpg)

![](jan_26_f.jpg)

![](jan_26_g.jpg)

The view on top of the castle, I could see the CN tower from here.
![](jan_26_h.jpg)

Back of door.
![](jan_26_i.jpg)

