# 11/29

### Last Day of This Session

Today was the last day of this session, we took the pictures with everyone in the class.

Morning Class
![](nov_28_a.jpg)

Afternoon Class
![](nov_28_b.jpg)

After school, we had the chick burger
![](nov_28_c.jpg)

Number 1 set
![](nov_28_d.jpg)

![](nov_28_e.jpg)

Waffle Potato Fries
![](nov_28_f.jpg)

Chick burger and ice tea
![](nov_28_g.jpg)

[midtown gastro hub](https://goo.gl/maps/bM6Ju1NqifPoDHPo9)

We went to a pub at night, there are different discounts everyday
![](nov_28_h.jpg)