# 11/17

There was a Santa Claus Parade, but the parade was over when I arrived there. So Abby, Lisa and I went to the Tim Hortons, we had a gossip, and then we went to the [Kensington Market](https://goo.gl/maps/92P7ubkwQFLEmU2u5) to try the real Tacos!

今天有聖誕遊行，但我抵達時已經結束，我跟 Abby, Lisa 去 Tim Hortons 閒聊，之後去 Kensington Market 吃正統 Tacos

### [Seven Lives Tacos Y Mariscos](https://goo.gl/maps/cqmb2ypFyta1WCHW8)

The shop

店門口

![](nov_17_a.jpg)

Logo

![](nov_17_b.jpg)

The menu, we ordered one BAJA FISH Taco

菜單，我們點了 BAJA FISH 玉米餅

![](nov_17_c.jpg)

![](nov_17_e.jpg)

![](nov_17_d.jpg)

![](nov_17_f.jpg)

The Taco is here!!!

Taco 上菜!!!

![](nov_17_g.jpg)

Took a pic with the chef.

跟廚師拍照

![](nov_17_h.jpg)

There were four different spicy sources you can add with Taco.

四種不同的辣椒

![](nov_17_i.jpg)

We were satisfied when we fished it.

吃飽後滿足～

![](nov_17_k.jpg)