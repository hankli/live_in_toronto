# 11/20

Today we went to [the Distillery District](https://www.thedistillerydistrict.com/), there was a Christmas Market on there, it was a fantastic experience , let's take a look.

### [Toronto Christmas Market](http://tochristmasmarket.com/)

the entrance

入口
![](./nov_20_a.jpg)

The first vendors after we enter from the entrance.

門口進來後的第一個攤販，頗有聖誕節的味道
![](./nov_20_b.jpg)

We saw a huge Christmas tree On the market street.

市集大街上出現一顆超大聖誕樹
![](./nov_20_c.jpg)

![](./nov_20_d.jpg) 

![](./nov_20_e.jpg)

The christmast tree, it was huge and beautiful.

聖誕樹本人，超大顆，很漂亮

![](./nov_20_f.jpg)

The choir sang the songs about Christmas.

合唱團演唱聖誕節相關歌曲

![](./nov_20_g.jpg)

![](./nov_20_h.jpg)

It was more beautiful at night.

越夜越美麗

![](./nov_20_i.jpg)