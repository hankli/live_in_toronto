# 12/27 Day1

### The Niagara Falls

Abby, Jiyoon, Tim and I were going to Niagara Falls by [GO Train](https://www.niagaraparks.com/visit-niagara-parks/plan-your-visit/deals-toronto/?utm_medium=web_referral&utm_source=go_transit&utm_campaign=go_niagara&utm_content=niagara_landing_page). We bought the [Niagara Falls Wonder Pass](https://www.niagaraparks.com/visit-niagara-parks/plan-your-visit/deals-packages-2019/?utm_expid=.bQQ5hHgwS3GditHtWdc-AA.1&utm_referrer=https%3A%2F%2Fwww.google.com%2F) which included **Journey Behind the Falls**, **Butterfly Conservatory**, **Niagara's Fury**, **Floral Showhouse**, **Falls Incline Railway** five places we can visit.

It's worth to buy the wonder pass, because the five places you can visit also WEGO bus ticket.

The Union Station, which is the station We went to Niagara Falls.
![](dec_27_a.jpg)

The tree made by paper.
![](dec_27_b.jpg)

Jiyoon, Abby, Tim and I.
![](dec_27_c.jpg)

We were sleeping while we were on the train.
![](dec_27_d.jpg)

This was the GO train we take.
![](dec_27_e.jpg)

![](dec_27_r.MP4)

We wnet to see the firework first but we got the bad place because there were too much people and it was too late to the good place to watch the firework.
![](dec_27_f.jpg)

Finally, we saw one of the Niagara Falls.
![](dec_27_k.jpg)

Second one.
![](dec_27_g.jpg)

Everybody took a picture in front of the falls.
![](dec_27_h.jpg)

![](dec_27_i.jpg)

We went to a place which had some devices you can play. It would show the animation when you push and pull the handle.
![](dec_27_l.MP4)

![](dec_27_m.MP4)

![](dec_27_q.jpg)

[Skylon tower](https://www.skylon.com/)
![](dec_27_j.jpg)

Observation Wheel, which was huge and move fast.
![](dec_27_n.jpg)

The street in the Niagara Falls. Like a never sleep city, even it was 12 am, there were many people on the street.
![](dec_27_o.jpg)

![](dec_27_p.jpg)


