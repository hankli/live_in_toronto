# 12/31

### New Year's Eve

Today was the last day of 2019. My classmates and I went to karaoke to sing the songs, had dinner, saw the firework in front of the city hall and drinked beer in a bar.

The karaoke, which is close to school, had songs from different lanuage, like Taiwanese, Japanese, English, etc. Really cool.
![](dec_31_a.jpg)

![](dec_31_b.jpg)

We waited for the firework in front of the city hall. People mountain people sea there.
![](dec_31_c.jpg)

Thank you everyone, I had a great time of 2019.
![](dec_31_d.jpg)