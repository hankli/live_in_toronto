# 12/28 Day2

### The Niagara Falls

This was the second day of the Niagara Falls. We visited every places in wonder pass except the Floral Showhouse.

Niagara Falls in the morning.
![](dec_28_a.jpg)

This bridge is the boarder between Canada and USA.
![](dec_28_b.jpg)

We were going to the Journey Behind the Falls. We were in the tunnel.
![](dec_28_c.jpg)

Waited for about 30 minutes, we finally saw the falls very closed.
![](dec_28_d.jpg)

There were a lot of coins in the water. I guessed they were throwed by the Asian.
![](dec_28_e.jpg)

We took a picture in front of the falls.
![](dec_28_k.jpg)

![](dec_28_f.jpg)

We took the Falls Incline Railway, which is a short railway.
![](dec_28_g.MP4)

After that we went to the Butterfly Conservatory. There were a lot of butterflies in the greenhouse.
![](dec_28_h.MP4)

At the end we backed to the hotel to get our luggages and ready to home by GO train.
![](dec_28_j.jpg)

We played the lottery in the game house.
![](dec_28_i.MP4)