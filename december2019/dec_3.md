# 12/3

### NBA Game

Watching a NBA game was a dream since I was child. Finally, I went to the NBA game yesterday (Miami Heat vs Toronto Raptors), the atmosphere of the game was amazing, even Raptors didn't win, I still felt so good. Let's go! Raptors!

Scotiabank Arena, it is the arena of the Raptors
![](dec_3_a.jpg)

Crowded people who were going to watch the game.
![](dec_3_b.jpg)

![](dec_3_c.jpg)

![](dec_3_d.jpg)

![](dec_3_e.jpg)

I got a Raptors key chain.
![](dec_3_f.jpg)

The court with me. It proved I was here.
![](dec_3_g.jpg)

The video of the reverse.
![](dec_3_h.mp4)