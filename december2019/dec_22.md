# 12/22 Day2

### the Blue Mountain Snowboarding trip

The second day of the snowboarding trip. Today we snowboarded all day. In the afternoon We had sushi for lunch. At night we bought Wendy's for dinner and backed to hotel to play the board game, which name is "狼人殺".

The hotel in the morning.
![](dec_22_a.jpg)

I was a beginner but I didn't have a course of snowboard. My friend tought me how to do it, I always fell down and I sat on the ground because I was so tired.
![](dec_22_b.jpg)

On the afternoon, we went to the village to have lunch, which is sushi, but I forgot to take the pictures.
![](dec_22_c.jpg)

![](dec_22_d.jpg)

![](dec_22_e.jpg)

About 5:30, we continued to snowboard. This was the view of the hill.
![](dec_22_f.jpg)

Close to end of the hill.
![](dec_22_g.jpg)

Finally, I did it.
![](dec_22_h.jpg)

Abby and I. Abby learned it so quickly.
![](dec_22_i.jpg)

It was so beautiful at night.
![](dec_22_j.jpg)
