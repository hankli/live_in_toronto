# 2/27

### [GDG Toronto Android](https://www.meetup.com/ToAndroidDev)

As an Android developer, it must go to local Android community. This was a meeting that hosted by GDG Android Toronto. Today, we had two topics about Kotlin, the one is Coroutine, the other one is Flow, they both were pretty new tech in Android. That was a great experience today.


![](feb_27_a.jpg)

![](feb_27_b.jpg)

![](feb_27_c.jpg)

![](feb_27_d.jpg)

![](feb_27_e.jpg)

![](feb_27_f.jpg)

![](feb_27_g.jpg)

![](feb_27_h.jpg)

![](feb_27_i.jpg)

![](feb_27_j.jpg)

![](feb_27_k.jpg)

![](feb_27_l.jpg)

![](feb_27_m.jpg)

![](feb_27_n.jpg)


