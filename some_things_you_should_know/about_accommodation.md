# 住宿篇

我在加拿大沒有朋友或親戚，所以我在寄宿住了 4 週，先熟悉環境，之後自行尋找房子

## 寄宿家庭 HomeStay

我的寄宿家庭是由學校安排，沒辦法指定，但是上學距離太遠可以請學校更換，因為食物、生活習慣會隨著家庭背景而不同，所以非常吃運氣（記得去拜拜）

如果有糾紛或問題可以先與寄宿家庭成員溝通，例如我剛到達的時候廁所很髒，跟 Home mom 溝通後有獲得改善，真的不適應可以更換寄宿家庭，我的寄宿家庭來自泰國，飲食上跟台灣接近

## 分租雅房 Share House
多倫多大致上分四區，北邊、東邊、西邊跟市中心．北區華人比較多，相對的華人超市及餐廳也會比較多．寄宿家庭結束後，我搬到西區 (Jane and Eglinton area)，這區歐洲人比較多，華人偏少，練習英文是個不錯的選擇．東區我比較少去，無法提供建議．市區最方便，應有盡有，但價格也是最高的．

## 租房

在多倫多租房子非常昂貴，一個月 550~850 CAD 都有，大多是雅房，如果想要套房或是更好的房子至少都要 1000 CAD 以上，當然根據地點也會有區別

這邊提供我在多倫多常用的租屋網站

[kijiji](https://www.kijiji.ca/)

[YorkBBS](http://www.yorkbbs.ca/)

[51 Canada](http://www.51.ca/)

[FB 加拿大打工度假資訊-租屋版](https://www.facebook.com/groups/140360422735205/)
