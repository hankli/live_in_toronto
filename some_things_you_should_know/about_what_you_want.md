# 目標篇

在打工度假中，每個人追求的目標不同，有些人想體驗國外生活，有些人想學好英文，有些人想存一桶金 等等...，建議將目標訂出來，對規劃很有幫助．

## 問問自己想要什麼

1. 為什麼想出國？
> - 體驗國外生活
> - 學好英文

2. 想去哪裡、去多久、什麼時候去？
> - 想去多倫多，三個月的語言學校、打工一年，預計 10 月出發

3. 預算多少？
> - 30 萬

4. 除了原本的目的還想得到什麼？
> - 說一口流利的英文
> - 加拿大 IT 相關工作經驗

5. 想要什麼樣的課程？
> - 口說、聽力加強
