# 工作篇

## 建議及準備

- 加拿大重視在地經驗，對於外國人要找到專業的工作並不容易，一開始可以先找一些好錄取的工作先做，累積一些經驗

- 英文能力不錯的話，可以在台灣就先上 LinkedIn, Indeed 先投履歷、面試

- 履歷跟 cover letter 很重要，一定要事先準備，如果可以也把推薦信準備起來

### 求職相關網站

- [加拿大最低時薪](http://srv116.services.gc.ca/dimt-wid/sm-mw/rpt1.aspx?GoCTemplateCulture=en-CA)
- [Linkin](https://www.linkedin.com)
- [indeed](https://www.indeed.ca/)
- [kijiji](https://www.kijiji.ca/)
- [glass door](https://www.glassdoor.ca) 練習面試問題、看薪水範圍
- [Leetcode](https://leetcode.com/) 練習網站
- [acces](http://accesemployment.ca/)
- [volunteer](https://www.volunteertoronto.ca/) 志工機會
- [ResumeCoach](https://www.resumecoach.com/) 線上履歷
